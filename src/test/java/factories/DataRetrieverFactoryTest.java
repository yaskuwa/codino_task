package factories;

import model.CsvFilePrefixes;
import org.junit.Test;
import retrievers.CyclistDataRetriever;
import retrievers.DogDataRetriever;
import retrievers.PersonDataRetriever;

import static org.junit.Assert.assertTrue;

public class DataRetrieverFactoryTest {

    @Test
    public void buildDataRetriever() {
        assertTrue(DataRetrieverFactory.buildDataRetriever(CsvFilePrefixes.CYCLIST) instanceof CyclistDataRetriever);
        assertTrue(DataRetrieverFactory.buildDataRetriever(CsvFilePrefixes.PERSON) instanceof PersonDataRetriever);
        assertTrue(DataRetrieverFactory.buildDataRetriever(CsvFilePrefixes.DOG) instanceof DogDataRetriever);
    }
}