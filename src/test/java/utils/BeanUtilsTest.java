package utils;

import factories.DataRetrieverFactory;
import model.CsvBean;
import model.CsvFilePrefixes;
import model.Person;
import org.junit.Before;
import org.junit.Test;
import retrievers.DataRetriever;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BeanUtilsTest {

    private List<CsvBean> beans;

    @Before
    public void setUp() throws Exception {
        String filePath = "src/test/resources/ludzie_w_warszawie.csv";
        DataRetriever retriever = DataRetrieverFactory.buildDataRetriever(CsvFilePrefixes.PERSON);
        beans = retriever.retrieveAllFromCsv(filePath);
    }

    @Test
    public void findById() {
        //when
        Optional<CsvBean> personOptional = BeanUtils.findById(beans, 5L);

        //then
        assertTrue(personOptional.isPresent());
        Person person = (Person) personOptional.get();
        assertEquals(5L, (long) person.getId());
        assertEquals("Zdzislaw", person.getName());
        assertEquals(48L, (long) person.getAge());
        assertEquals(7L, (long) person.getParentId());
    }

    @Test
    public void sortMapByListCountDesc() {
        //given
        Map<String, List<CsvBean>> beanMap = new HashMap<>();
        List<CsvBean> firstList = new ArrayList<>();
        firstList.add(beans.get(0));
        firstList.add(beans.get(1));
        firstList.add(beans.get(2));
        beanMap.put("key1", firstList);

        List<CsvBean> secondList = new ArrayList<>();
        secondList.add(beans.get(3));
        secondList.add(beans.get(4));
        secondList.add(beans.get(5));
        secondList.add(beans.get(6));
        secondList.add(beans.get(7));
        beanMap.put("key2", secondList);

        //when
        Map<String, List<CsvBean>> sortedMap = BeanUtils.sortMapByListCountDesc(beanMap);

        //then
        assertTrue(sortedMap instanceof LinkedHashMap);
        Set<String> keys = sortedMap.keySet();
        assertEquals("[key2, key1]", keys.toString());
    }
}