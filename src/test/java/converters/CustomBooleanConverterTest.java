package converters;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CustomBooleanConverterTest {

    private CustomBooleanConverter<Boolean> converter;
    private String falseString;
    private String trueString;

    @Before
    public void setUp() {
        converter = new CustomBooleanConverter<>();
        falseString = "-";
        trueString = "+";
    }

    @Test
    public void convert() throws CsvDataTypeMismatchException {
        //when
        boolean trueBoolean = (boolean) converter.convert(trueString);
        boolean falseBoolean = (boolean) converter.convert(falseString);

        //then
        assertTrue(trueBoolean);
        assertFalse(falseBoolean);
    }
}