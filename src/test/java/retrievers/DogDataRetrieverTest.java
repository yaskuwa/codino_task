package retrievers;

import factories.DataRetrieverFactory;
import model.CsvFilePrefixes;
import model.Dog;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DogDataRetrieverTest {

    private String filePath;
    private DataRetriever retriever;

    @Before
    public void setUp() {
        filePath = "src/test/resources/psy_schronisko.csv";
        retriever = DataRetrieverFactory.buildDataRetriever(CsvFilePrefixes.DOG);
    }

    @Test
    public void retrieveAllFromCsv() throws IOException {
        //when
        List<Dog> dogs = retriever.retrieveAllFromCsv(filePath);

        //then
        Dog dog = dogs.get(0);
        assertEquals(3, dogs.size());
        assertEquals(1001L, (long) dog.getId());
        assertEquals("Bialek", dog.getName());
        assertEquals(3L, (long) dog.getAge());
        assertTrue(dog.isVaccinated());
        assertEquals("Marcin", dog.getOwner());
        assertEquals(0L, (long) dog.getParentId());
    }
}