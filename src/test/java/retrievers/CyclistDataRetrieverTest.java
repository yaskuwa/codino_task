package retrievers;

import factories.DataRetrieverFactory;
import model.CsvFilePrefixes;
import model.Cyclist;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CyclistDataRetrieverTest {

    private String filePath;
    private DataRetriever retriever;

    @Before
    public void setUp() {
        filePath = "src/test/resources/rowerzysci_zawody.csv";
        retriever = DataRetrieverFactory.buildDataRetriever(CsvFilePrefixes.CYCLIST);
    }

    @Test
    public void retrieveAllFromCsv() throws IOException {
        //when
        List<Cyclist> cyclists = retriever.retrieveAllFromCsv(filePath);

        //then
        Cyclist cyclist = cyclists.get(0);
        assertEquals(3, cyclists.size());
        assertEquals(11L, (long) cyclist.getId());
        assertEquals("Anna", cyclist.getName());
        assertEquals("3h17min", cyclist.getRecord());
        assertEquals(21L, (long) cyclist.getAge());
        assertEquals("Łódź", cyclist.getCity());
        assertEquals(0L, (long) cyclist.getParentId());
    }
}