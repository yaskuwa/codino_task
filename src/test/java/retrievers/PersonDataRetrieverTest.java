package retrievers;

import factories.DataRetrieverFactory;
import model.CsvFilePrefixes;
import model.Person;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PersonDataRetrieverTest {

    private String filePath;
    private DataRetriever retriever;

    @Before
    public void setUp() {
        filePath = "src/test/resources/ludzie_w_warszawie.csv";
        retriever = DataRetrieverFactory.buildDataRetriever(CsvFilePrefixes.PERSON);
    }

    @Test
    public void retrieveAllFromCsv() throws IOException {
        //when
        List<Person> people = retriever.retrieveAllFromCsv(filePath);

        //then
        Person person = people.get(0);
        assertEquals(8, people.size());
        assertEquals(1L, (long) person.getId());
        assertEquals("Piotr", person.getName());
        assertEquals(47L, (long) person.getAge());
        assertEquals(5L, (long) person.getParentId());
    }
}