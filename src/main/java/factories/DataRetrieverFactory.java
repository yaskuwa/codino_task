package factories;

import model.CsvFilePrefixes;
import retrievers.CyclistDataRetriever;
import retrievers.DataRetriever;
import retrievers.DogDataRetriever;
import retrievers.PersonDataRetriever;

public class DataRetrieverFactory {
    public static DataRetriever buildDataRetriever(String filePrefix) {
        DataRetriever retriever = null;
        switch (filePrefix) {
            case CsvFilePrefixes.PERSON:
                retriever = new PersonDataRetriever();
                break;
            case CsvFilePrefixes.CYCLIST:
                retriever = new CyclistDataRetriever();
                break;
            case CsvFilePrefixes.DOG:
                retriever = new DogDataRetriever();
                break;
        }
        return retriever;
    }
}
