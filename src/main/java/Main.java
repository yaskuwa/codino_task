import handlers.ActionHandler;

public class Main {
    public static void main(String[] args) {
        ActionHandler actionHandler = new ActionHandler();
        actionHandler.handleInput(args);
    }
}
