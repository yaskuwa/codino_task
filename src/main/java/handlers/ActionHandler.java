package handlers;

import exceptions.IncorrectFileNameException;
import exceptions.WrongArgumentsException;
import factories.DataRetrieverFactory;
import model.CsvBean;
import retrievers.DataRetriever;
import utils.BeanUtils;
import validators.ArgumentsValidator;

import java.io.IOException;
import java.util.*;

public class ActionHandler {
    private final static String FILENAME_SPLITTER = "_";

    public void handleInput(String[] args) {
        try {
            ArgumentsValidator.validate(args);
            String fileName = args[0];
            String filePrefix = fileName.split(FILENAME_SPLITTER)[0];

            DataRetriever retriever = DataRetrieverFactory.buildDataRetriever(filePrefix);
            List<CsvBean> retrievedBeans = retriever.retrieveAllFromCsv(fileName);

            executeFunction(retrievedBeans, args);
        } catch (IncorrectFileNameException | IOException | WrongArgumentsException e) {
            System.err.println(e.getMessage());
        }
    }

    private void executeFunction(List<CsvBean> retrievedBeans, String[] args) {
        int functionNumber = Integer.parseInt(args[1]);
        switch (functionNumber) {
            case 1:
                printFirstTenElements(retrievedBeans);
                break;
            case 2:
                printNumberOfElements(retrievedBeans);
                break;
            case 3:
                printElementsWithIdHigherThan(retrievedBeans, Long.parseLong(args[2]));
                break;
            case 4:
                printNamesOccurringMoreThanHundred(retrievedBeans);
                break;
            case 5:
                printBeanNameAndNumberOfParents(retrievedBeans, Long.parseLong(args[2]));
                break;
            case 6:
                printAverageAge(retrievedBeans);
                break;
            case 7:
                printAgeDistribution(retrievedBeans);
                break;
        }
    }

    private void printAgeDistribution(List<CsvBean> retrievedBeans) {
        double maxAge = retrievedBeans.stream().mapToDouble(CsvBean::getAge).max().getAsDouble();
        double minAge = retrievedBeans.stream().mapToDouble(CsvBean::getAge).min().getAsDouble();
        int numberOfSections = 4;
        double ageSection = (maxAge - minAge) / numberOfSections;

        Map<Integer, List<CsvBean>> groupedByAge = new HashMap<>();
        for (int i = 0; i < numberOfSections; i++) {
            groupedByAge.put(i, new ArrayList<>());
        }
        retrievedBeans.forEach(bean -> {
            double sectionNumber = ((bean.getAge() - minAge) / ageSection);
            if (sectionNumber == Math.floor(sectionNumber) && sectionNumber != 0)
                sectionNumber -= 1;
            groupedByAge.get((int) sectionNumber).add(bean);
        });
        groupedByAge.forEach((key, value) -> {
            double sectionMin = minAge + key * ageSection;
            double sectionMax = minAge + (key + 1) * ageSection;
            int percentage = (int) Math.round(100 * (double) value.size() / retrievedBeans.size());
            System.out.println(sectionMin + " - " + sectionMax + " : " + percentage + "%");
        });
    }

    private void printAverageAge(List<CsvBean> retrievedBeans) {
        System.out.printf("Average age - %.2f \n", retrievedBeans.stream().mapToDouble(CsvBean::getAge).average().getAsDouble());
    }

    private void printBeanNameAndNumberOfParents(List<CsvBean> retrievedBeans, long id) {
        Optional<CsvBean> selectedBean = retrievedBeans.stream().filter(bean -> bean.getId().equals(id)).findFirst();
        if (selectedBean.isEmpty()) {
            System.out.println("No entry with id=" + id + " found.");
        }
        selectedBean.ifPresent(bean -> {
            long parentId = bean.getParentId();
            int numberOfParents = 0;
            while (parentId != 0) {
                Optional<CsvBean> parent = BeanUtils.findById(retrievedBeans, parentId);
                if (parent.isPresent()) {
                    numberOfParents += 1;
                    parentId = parent.get().getParentId();
                } else {
                    System.out.println("Wrong parent id=" + parentId);
                    break;
                }
            }
            System.out.println(bean.getName() + " - " + numberOfParents);
        });
    }

    private void printNamesOccurringMoreThanHundred(List<CsvBean> retrievedBeans) {
        Map<String, List<CsvBean>> groupedByName = new HashMap<>();
        retrievedBeans.forEach(bean -> {
            if (!groupedByName.containsKey(bean.getName())) {
                List<CsvBean> namedBeans = new ArrayList<>();
                namedBeans.add(bean);
                groupedByName.put(bean.getName(), namedBeans);
            } else {
                groupedByName.get(bean.getName()).add(bean);
            }
        });

        groupedByName.entrySet().removeIf(entry -> entry.getValue().size() <= 100);
        Map<String, List<CsvBean>> sorted = BeanUtils.sortMapByListCountDesc(groupedByName);
        sorted.forEach((key, value) -> System.out.println(key + " " + value.size()));
    }

    private void printElementsWithIdHigherThan(List<CsvBean> retrievedBeans, Long id) {
        System.out.println("Elements with id higher than " + id + ":");
        retrievedBeans.stream()
                .filter(bean -> bean.getId() > id)
                .sorted(Comparator.comparing(CsvBean::getId))
                .forEach(System.out::println);
    }

    private void printNumberOfElements(List<CsvBean> retrievedBeans) {
        System.out.println("Number of entries: " + retrievedBeans.size());
    }

    private void printFirstTenElements(List<CsvBean> retrievedBeans) {
        System.out.println("First 10 entries:");
        retrievedBeans.stream().limit(10).forEach(System.out::println);
    }

}
