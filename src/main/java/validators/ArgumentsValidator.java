package validators;

import exceptions.IncorrectFileNameException;
import exceptions.WrongArgumentsException;
import model.CsvFilePrefixes;

public class ArgumentsValidator {
    private static final String CSV_SUFFIX = ".csv";

    public static void validate(String[] args) throws IncorrectFileNameException, WrongArgumentsException {
        validateArguments(args);
        String fileName = args[0];
        validateFileName(fileName);
    }

    private static void validateArguments(String[] args) throws WrongArgumentsException {
        if(!(args.length == 2) && !(args.length == 3)) {
            throw new WrongArgumentsException(WrongArgumentsException.WRONG_ARGUMENT_NUMBER);
        } else {
            int functionNumber = Integer.parseInt(args[1]);
            if(functionNumber < 1 || functionNumber > 7) {
                throw new WrongArgumentsException(WrongArgumentsException.WRONG_FUNCTION_NUMBER);
            } else {
                if((functionNumber == 3 || functionNumber == 5) && args.length != 3){
                    throw new WrongArgumentsException(WrongArgumentsException.WRONG_ARGUMENT_NUMBER_FOR_FUNCTION);
                } else if((functionNumber == 3 || functionNumber == 5) && args.length == 3) {
                    try {
                        Long.parseLong(args[2]);
                    } catch(NumberFormatException e) {
                        throw new WrongArgumentsException(WrongArgumentsException.WRONG_TYPE);
                    }
                }
            }
        }
    }

    private static void validateFileName(String fileName) throws IncorrectFileNameException {
        if(fileName.endsWith(CSV_SUFFIX)){
            if(!fileName.startsWith(CsvFilePrefixes.PERSON)
                    && !fileName.startsWith(CsvFilePrefixes.CYCLIST)
                    && !fileName.startsWith(CsvFilePrefixes.DOG)) {
                throw new IncorrectFileNameException(String.format(IncorrectFileNameException.WRONG_FILENAME, fileName));
            }
        } else {
            throw new IncorrectFileNameException(String.format(IncorrectFileNameException.WRONG_FILENAME, fileName));
        }
    }
}
