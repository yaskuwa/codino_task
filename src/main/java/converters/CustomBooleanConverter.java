package converters;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import lombok.NoArgsConstructor;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.converters.BooleanConverter;
import org.apache.commons.lang3.StringUtils;

import java.util.ResourceBundle;

@NoArgsConstructor
public class CustomBooleanConverter<T> extends AbstractBeanField<T> {
    protected static final String[] TRUE_STRINGS = {"+", "true"};
    protected static final String[] FALSE_STRINGS = {"-", "false"};

    public Object convert(String value) throws CsvDataTypeMismatchException {
        if (StringUtils.isEmpty(value)) {
            return null;
        } else {
            BooleanConverter bc = new BooleanConverter(TRUE_STRINGS, FALSE_STRINGS);

            try {
                return bc.convert(Boolean.class, value.trim());
            } catch (ConversionException e) {
                CsvDataTypeMismatchException csve = new CsvDataTypeMismatchException(value, this.field.getType(), ResourceBundle.getBundle("customBooleanConverter", this.errorLocale).getString("input.not.boolean"));
                csve.initCause(e);
                throw csve;
            }
        }
    }

    protected String convertToWrite(Object value) throws CsvDataTypeMismatchException {
        String result = "";

        try {
            if (value != null) {
                Boolean b = (Boolean)value;
                result = b ? "+" : "-";
            }

            return result;
        } catch (ClassCastException e) {
            CsvDataTypeMismatchException csve = new CsvDataTypeMismatchException(ResourceBundle.getBundle("customBooleanConverter", this.errorLocale).getString("field.not.boolean"));
            csve.initCause(e);
            throw csve;
        }
    }
}
