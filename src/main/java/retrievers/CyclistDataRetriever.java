package retrievers;

import lombok.NoArgsConstructor;
import model.Cyclist;

@NoArgsConstructor
public class CyclistDataRetriever extends DataRetriever<Cyclist> {

}
