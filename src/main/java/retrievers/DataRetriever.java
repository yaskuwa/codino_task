package retrievers;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import model.CsvBean;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.ParameterizedType;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public abstract class DataRetriever<T extends CsvBean> {

    final Class<T> typeParameterClass;

    public DataRetriever() {
        this.typeParameterClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public List<T> retrieveAllFromCsv(String fileName) throws IOException {
        List<T> beans = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader(Paths.get(fileName))) {
            CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                    .withType(typeParameterClass)
                    .build();
            beans.addAll(csvToBean.parse());
        }
        return beans;
    }

}
