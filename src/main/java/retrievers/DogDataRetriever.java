package retrievers;

import lombok.NoArgsConstructor;
import model.Dog;

@NoArgsConstructor
public class DogDataRetriever extends DataRetriever<Dog> {

}
