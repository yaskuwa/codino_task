package retrievers;

import lombok.NoArgsConstructor;
import model.Person;

@NoArgsConstructor
public class PersonDataRetriever extends DataRetriever<Person> {

}
