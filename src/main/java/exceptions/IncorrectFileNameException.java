package exceptions;

public class IncorrectFileNameException extends Exception {
    public static final String WRONG_FILENAME = "Incorrect file name : %s";

    public IncorrectFileNameException(String message) {
        super(message);
    }
}
