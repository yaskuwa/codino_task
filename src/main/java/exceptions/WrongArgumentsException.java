package exceptions;

public class WrongArgumentsException extends Exception {

    public static final String WRONG_ARGUMENT_NUMBER = "Wrong number of arguments! You need to input 2 or 3 arguments.";
    public static final String WRONG_ARGUMENT_NUMBER_FOR_FUNCTION = "Wrong number of arguments! The selected function requires one additional argument.";
    public static final String WRONG_FUNCTION_NUMBER = "Wrong function number. Please select a function between 1 and 7.";
    public static final String WRONG_TYPE = "Third argument needs to be a number!";

    public WrongArgumentsException(String message) {
        super(message);
    }
}
