package utils;

import model.CsvBean;

import java.util.*;
import java.util.stream.Collectors;

public class BeanUtils {
    public static Optional<CsvBean> findById(List<CsvBean> retrievedBeans, long id){
        return retrievedBeans.stream()
                .filter(b -> b.getId().equals(id))
                .findFirst();
    }

    public static Map<String, List<CsvBean>> sortMapByListCountDesc(Map<String, List<CsvBean>> map) {
        return map.entrySet().stream()
                .sorted(Comparator.comparing(stringListEntry -> stringListEntry.getValue().size(), Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> {
                            throw new AssertionError();
                        },
                        LinkedHashMap::new
                ));
    }
}
