package model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import converters.CustomBooleanConverter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class Dog extends CsvBean {

    @CsvBindByName
    private String owner;

    @CsvCustomBindByName(column = "vaccine", converter = CustomBooleanConverter.class)
    private boolean vaccinated;

    @Override
    public String toString() {
        return "Dog{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", vaccine=" + isVaccinated() +
                ", owner=" + getOwner() +
                ", parentId=" + getParentId() +
                '}';
    }
}
