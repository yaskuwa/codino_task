package model;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class Person extends CsvBean {

    @CsvBindByName
    private String surname;

    @CsvBindByName
    private String city;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", surname=" + getSurname() +
                ", age=" + getAge() +
                ", city=" + getCity() +
                ", parentId=" + getParentId() +
                '}';
    }
}
