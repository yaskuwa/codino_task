package model;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class Cyclist extends CsvBean {

    @CsvBindByName
    private String city;

    @CsvBindByName(column = "rekord")
    private String record;

    @Override
    public String toString() {
        return "Cyclist{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", city=" + getCity() +
                ", rekord=" + getRecord() +
                ", parentId=" + getParentId() +
                '}';
    }
}
