package model;

public class CsvFilePrefixes {
    public static final String PERSON = "ludzie";
    public static final String CYCLIST = "rowerzysci";
    public static final String DOG = "psy";
}
